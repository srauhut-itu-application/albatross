# -*- coding: utf-8 -*-

# University of Potsdam
# Semester Project
# S. Rauhut

"""This file contains all utterances for the Albatross game skill."""

# SSML tags
tag_general_begin = '<speak>'
tag_general_end = '</speak>'
tag_voice_begin = '<voice name="Emma">'
tag_voice_end = '</voice>'
tag_pause = '<break time="3s"/>'
tag_pause_short = '<break time="2s"/>'
tag_pause_tiny = '<break time="0.08s"/>'


# Utterances that are not part of the game and are uttered by Alexa
utt_meta_greeting = "Welcome to Albatross. Albatross is a dialog-based science-fiction game. You can start the game now or ask for help."
utt_meta_reprompt_start = "You can tell me to start the game!"

utt_meta_saystartover = "To start over, say start over."
utt_meta_saystartgame = "To start the game, say start the game."

utt_meta_help = "You are the protagonist of Albatross. You win the game by issuing voice commands to the side character to reach a goal. For more information, talk to the side character in-game! "
utt_meta_helpingame = utt_meta_help + utt_meta_saystartover
utt_meta_helpoutside = utt_meta_help + utt_meta_saystartgame
utt_meta_helpreprompt = "Let me know if you need help!"
utt_meta_cannothelp = "The Albatross skill cannot help you with that."
utt_meta_exception = "Sorry, I did not understand. Please try again."

utt_meta_alreadyingame = "You have already started the game. " + utt_meta_saystartover
utt_meta_notingame = "You have not started the game yet. " + utt_meta_saystartgame
utt_meta_ingamecommand = "It seems you are trying to use an in-game command, but you are not currently in the game. " + utt_meta_saystartgame

utt_meta_launchinggame = "Launching the game now. "
utt_meta_startingover = "Restarting the game now. "
utt_meta_quit = "The game has been closed. You can start a new game at any time."
utt_meta_savegame = "Sorry, this skill does not yet have the functionality to save a game."
utt_meta_goal = "Your goal is to activate a communication system with the help of the secondary character. Talk to the character in game for more information."

utt_meta_youhavewon = "Congratulations! You have beat Albatross! More levels will be added in the future. Until then, you can replay the game as often as you like."

utt_drone_greeting = "Commander. You have waken up. I was not sure whether you would make it."

utt_gamestart = tag_general_begin + utt_meta_launchinggame + tag_pause + tag_voice_begin + utt_drone_greeting + tag_voice_end + tag_general_end
utt_gamerestart = tag_general_begin + utt_meta_startingover + tag_pause + tag_voice_begin + utt_drone_greeting + tag_voice_end + tag_general_end

# Utterances that are part of the game and uttered by Julia
utt_drone_whereami = tag_general_begin + tag_voice_begin + "You are on the bridge of the starship " + tag_pause_tiny + "Albatross." + tag_voice_end + tag_general_end
utt_drone_whoami = tag_general_begin + tag_voice_begin + "You are the Commander of the starship " + tag_pause_tiny + "Albatross." + tag_voice_end + tag_general_end
utt_drone_whathappened = tag_general_begin + tag_voice_begin + "There was an unspecified accident on the ship. All of the crew, including you, fell unconscious and the ship's external communications system shut down." + tag_voice_end + tag_general_end
utt_drone_whoareyou = tag_general_begin + tag_voice_begin + "I am " + '<emphasis>Julia, </emphasis>' + "the ship's assistance drone. I can answer questions and execute simple commands using my mechanical arms. We can communicate over the ship's speakers, which are still intact." + tag_voice_end + tag_general_end
utt_drone_goal = tag_general_begin + tag_voice_begin + "Your objective is to reactivate communications so the ship can send a distress call for help. Since you are unable to move, you will need to tell me exactly what to do. I am able to move and execute simple commands using my mechanical arms." + tag_voice_end + tag_general_end
utt_drone_comms = tag_general_begin + tag_voice_begin + "The ship's communications can be activated by inserting the three physical security keys into the terminal on the bridge. I can retrieve and insert the keys for you, if you tell me preciely where to go." + tag_voice_end + tag_general_end
utt_drone_keylocations = tag_general_begin + tag_voice_begin + "One key is located in the ship's laboratory, another in the engine room, and the third in the commander's quarters." + tag_voice_end + tag_general_end
utt_drone_location = tag_general_begin + tag_voice_begin + "My current location is the "
utt_drone_keysfound = tag_general_begin + tag_voice_begin + "I am in possession of "
utt_drone_nokeys = tag_general_begin + tag_voice_begin + "I am not currently in possession of any keys." + tag_voice_end + tag_general_end
utt_drone_notenoughkeys = "I do not have all three keys required to activate communications."
utt_drone_enoughkeys = tag_general_begin + tag_voice_begin + "While I am in possession of all keys required to activate communications, "
utt_drone_notonbridge = "I need to be on the bridge to do so." + tag_voice_end + tag_general_end
utt_drone_amonbridge = tag_general_begin + tag_voice_begin + "While I am on the bridge and can access the terminal, "
utt_drone_activatingnow = "Activating communications now. Help will be on its way soon. Good work, Commander. " + tag_voice_end
utt_drone_nokeyhere = tag_general_begin + tag_voice_begin + "There is no key in this room." + tag_voice_end + tag_general_end
utt_drone_alreadypickedup = tag_general_begin + tag_voice_begin + "I have already picked up the key in this room." + tag_voice_end + tag_general_end
utt_drone_pickingup = tag_general_begin + tag_voice_begin + "Picking up the key. " + tag_pause_short + "Key picked up." + tag_voice_end + tag_general_end
utt_drone_goingtoroom = tag_general_begin + tag_voice_begin + "I am on my way. " + tag_pause_short + "I have arrived." + tag_voice_end + tag_general_end
utt_drone_alreadyinroom = tag_general_begin + tag_voice_begin + "I am already in that room. Let me know if you would like me to move somewhere else." + tag_voice_end + tag_general_end
utt_drone_nosuchroom = tag_general_begin + tag_voice_begin + "There is no such room on this ship." + tag_voice_end + tag_general_end
