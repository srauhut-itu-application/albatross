# -*- coding: utf-8 -*-

# University of Potsdam
# Semester Project
# S. Rauhut

# Handler Classes used are provided by Amazon Alexa

"""This is the main lambda file for the Amazon Alexa Skill Albatross,
where all intents are handled."""

# Import sdk modules
import logging
from ask_sdk_core.skill_builder import SkillBuilder

from ask_sdk_core.dispatch_components import AbstractRequestHandler
from ask_sdk_core.dispatch_components import AbstractExceptionHandler
from ask_sdk_core.utils import is_request_type, is_intent_name
from ask_sdk_core.handler_input import HandlerInput

from ask_sdk_model.ui import SimpleCard
from ask_sdk_model import Response

# Import custom modules
import utterances
import gamekeeper

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

sb = SkillBuilder()

# Initialize gamekeeper class member
# The gamekeeper will keep track of all game-state variables
gamekeeper = gamekeeper.Gamekeeper()


class LaunchRequestHandler(AbstractRequestHandler):
    """Handler for Skill Launch. Invocation: Albatross Game"""
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("LaunchRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        # The text for the greeting, like all utterances, is pulled from the utterances module
        speech_text = utterances.utt_meta_greeting
        reprompt = utterances.utt_meta_reprompt_start

        handler_input.response_builder.speak(speech_text).set_card(
            SimpleCard("Albatross", speech_text)).set_should_end_session(
            False)
        return handler_input.response_builder.response

class StartGameIntentHandler(AbstractRequestHandler):
    """Handler for Game Start within the Skill, after the Skill has already been started."""
    def can_handle(self, handler_input):
        return is_intent_name("Meta_StartGame")(handler_input)

    def handle(self, handler_input):
        # If the game is running already, reset the game state and begin the game anew
        if gamekeeper.game_is_on:
            gamekeeper.clearall()
            speech_text = utterances.utt_gamerestart
        # If the game is not running, start it now
        else:
            gamekeeper.game_is_on = True
            speech_text = utterances.utt_gamestart

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class ExitGameIntentHandler(AbstractRequestHandler):
    """Handler for quitting the game, but not the Skill. Quitting the entire Skill is handled by the Amazon-provided Exit Intent"""
    def can_handle(self, handler_input):
        return is_intent_name("Meta_ExitGame")(handler_input)

    def handle(self, handler_input):
        # If the game is running, reset the game state and set the game to not running
        if gamekeeper.game_is_on:
            gamekeeper.clearall()
            gamekeeper.game_is_on = False
            speech_text = utterances.utt_meta_quit
        # Inform the user if the game is not running
        else:
            speech_text = utterances.utt_meta_notingame

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class SaveGameIntentHandler(AbstractRequestHandler):
    """Handler for request to save the game. Currently the game cannot be saved, but the user's wish to do so is recognized for a later implementation of this feature."""
    def can_handle(self, handler_input):
        return is_intent_name("Meta_SaveGame")(handler_input)

    def handle(self, handler_input):
        # Inform the user that saving the game is not currently possible
        # This is were the save-game feature will be implemented in a later version
        speech_text = utterances.utt_meta_savegame

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class WhereAmIIntentHandler(AbstractRequestHandler):
    """Handler for when the player asks about their location in the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_WhereAmI")(handler_input)

    def handle(self, handler_input):
        # If the game is running, have Julia explain that the protagonist is on a star ship
        if gamekeeper.game_is_on:
            speech_text = utterances.utt_drone_whereami
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class WhoAmIIntentHandler(AbstractRequestHandler):
    """Handler for Who Am I Question within the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_WhoAmI")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            # Have Julia explain that the protagonist is the commander of the ship
            speech_text = utterances.utt_drone_whoami
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class WhatHappenedIntentHandler(AbstractRequestHandler):
    """Handler for Who Am I Question within the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_WhatHappened")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            # Have Julia explain there was an accident
            speech_text = utterances.utt_drone_whathappened
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class WhatIsMyGoalIntentHandler(AbstractRequestHandler):
    """Handler for questions about the goal of the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_WhatIsMyGoal")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            # Have Julia explain the goal of the game, i. e. activating comms
            speech_text = utterances.utt_drone_goal
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            # If the game is not running, have Alexa explain the goal without giving as much away as Julia would
            speech_text = utterances.utt_meta_goal

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class WhoAreYouIntentHandler(AbstractRequestHandler):
    """Handler for Who Are You Question within the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_WhoAreYou")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            # Have Julia present herself
            speech_text = utterances.utt_drone_whoareyou
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class HowToActivateIntentHandler(AbstractRequestHandler):
    """Handler for questions on how to activate communications within the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_HowToActivateComms")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            # Inform the player how the communication system can be activated
            speech_text = utterances.utt_drone_comms
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class WhereAreTheKeysIntentHandler(AbstractRequestHandler):
    """Handler for questions on the key locations within the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_WhereAreTheKeys")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            # Have Julia name all rooms where keys are located
            # This is static for the moment but might be made random in a later version
            speech_text = utterances.utt_drone_keylocations
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class WhereAreYouIntentHandler(AbstractRequestHandler):
    """Handler for questions on how to activate communications within the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_WhereAreYou")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            speech_text = utterances.utt_drone_location + gamekeeper.drone_location + "." + utterances.tag_voice_end + utterances.tag_general_end
            # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class KeysFoundIntentHandler(AbstractRequestHandler):
    """Handler for questions on how to activate communications within the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Question_WhichKeysAreFound")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            # Check if Julia has found any keys
            if gamekeeper.keys_found:
                # If she has one key, have her name the one key
                if len(gamekeeper.keys_found) == 1:
                    speech_text = utterances.utt_drone_keysfound + gamekeeper.keys_found[0] + "." + utterances.tag_voice_end + utterances.tag_general_end
                # If she has two keys, name both separated with and
                elif len(gamekeeper.keys_found) == 2:
                    speech_text = utterances.utt_drone_keysfound + gamekeeper.keys_found[0] + " and " + gamekeeper.keys_found[1] + "." + utterances.tag_voice_end + utterances.tag_general_end
                # If she has three keys, name all three separated with comma and and
                elif len(gamekeeper.keys_found) == 3:
                    speech_text = utterances.utt_drone_keysfound + gamekeeper.keys_found[0] + ", " + gamekeeper.keys_found[1] + " and " + gamekeeper.keys_found[2] + "." + utterances.tag_voice_end + utterances.tag_general_end
                # Julia should never have more than three keys
                else:
                    speech_text = "Error: Disallowed number of keys."
            # If Julia has not found any keys yet, let the player know
            else:
                speech_text = utterances.utt_drone_nokeys
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class ActivateCommsIntentHandler(AbstractRequestHandler):
    """Handler for questions on how to activate communications within the game."""
    def can_handle(self, handler_input):
        return is_intent_name("Command_ActivateComms")(handler_input)

    def handle(self, handler_input):
        #  Check to make sure the game is running
        if gamekeeper.game_is_on:
            # Check if Julia has all three keys
            if gamekeeper.found_all:
                # Check if Julia is on the bridge
                if gamekeeper.drone_location == gamekeeper.BRIDGE:
                    # If both is given, have her activate comms and congratulate the player
                    speech_text = utterances.tag_general_begin + utterances.tag_voice_begin + utterances.utt_drone_activatingnow + utterances.tag_pause + utterances.utt_meta_youhavewon + utterances.tag_general_end
                    # End game and reset game state
                    gamekeeper.game_is_on = False
                    gamekeeper.clearall()
                # If she has all keys, but is not on the bridge, inform the player of this
                else:
                    speech_text = utterances.utt_drone_enoughkeys + utterances.utt_drone_notonbridge
            # If Julia does not have all keys:
            else:
                # If she is on the bridge, inform the player she needs more keys
                if gamekeeper.drone_location == gamekeeper.BRIDGE:
                    speech_text = utterances.utt_drone_amonbridge + utterances.utt_drone_notenoughkeys + utterances.tag_voice_end + utterances.tag_general_end
                # If she is not on the bridge, explain the winning conditions to the player
                else:
                    speech_text = utterances.tag_general_begin + utterances.tag_voice_begin + utterances.utt_drone_notenoughkeys + " Furthermore, " + utterances.utt_drone_notonbridge
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class PickUpKeyIntentHandler(AbstractRequestHandler):
    """Handler for the command to pick up keys."""
    def can_handle(self, handler_input):
        return is_intent_name("Command_PickUpKey")(handler_input)

    def handle(self, handler_input):
        # Check to make sure the game is running
        if gamekeeper.game_is_on:
            if gamekeeper.drone_location == gamekeeper.LAB:
                if gamekeeper.found_lab:
                    # If Julia is in the lab and has already picked up the key in the lab, inform the user of this
                    speech_text = utterances.utt_drone_alreadypickedup
                else:
                    # If she hasn't yet picked up the key, have her pick up the key and add it to her inventory
                    speech_text = utterances.utt_drone_pickingup
                    gamekeeper.pickupkey(gamekeeper.LAB_KEY)
                    gamekeeper.found_lab = True
            elif gamekeeper.drone_location == gamekeeper.ENGINE_ROOM:
                if gamekeeper.found_engine:
                    # If Julia is in the lab and has already picked up the key in the lab, inform the user of this
                    speech_text = utterances.utt_drone_alreadypickedup
                else:
                    # If she hasn't yet picked up the key, have her pick up the key and add it to her inventory
                    speech_text = utterances.utt_drone_pickingup
                    gamekeeper.pickupkey(gamekeeper.ENGINE_KEY)
                    gamekeeper.found_engine = True
            elif gamekeeper.drone_location == gamekeeper.QUARTERS:
                if gamekeeper.found_quarters:
                    # If Julia is in the lab and has already picked up the key in the lab, inform the user of this
                    speech_text = utterances.utt_drone_alreadypickedup
                else:
                    # If she hasn't yet picked up the key, have her pick up the key and add it to her inventory
                    speech_text = utterances.utt_drone_pickingup
                    gamekeeper.pickupkey(gamekeeper.QUARTERS_KEY)
                    gamekeeper.found_quarters = True
            else:
                speech_text = utterances.utt_drone_nokeyhere
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class GoToRoomIntentHandler(AbstractRequestHandler):
    """Handler for commands to move to a room."""
    def can_handle(self, handler_input):
        return is_intent_name("Command_GoToRoom")(handler_input)

    def handle(self, handler_input):
        # If user is currently in the game:
        if gamekeeper.game_is_on:
            # Read the slot value for the room slot
            current_intent = handler_input.request_envelope.request.intent
            for slot_name, current_slot in current_intent.slots.items():
                room = current_slot.resolutions.resolutions_per_authority[0].values[0].value.name
                # If the value is the room Julia is already in, inform the user of this
                if room == gamekeeper.drone_location:
                    speech_text = utterances.utt_drone_alreadyinroom
                # Otherwise have Julia go to the room
                # Set Julia's location to that room
                else:
                    speech_text = utterances.utt_drone_goingtoroom
                    gamekeeper.drone_location = room
        # If the game is not running, inform the user that they need to start the game in order to use in-game commands
        else:
            speech_text = utterances.utt_meta_ingamecommand

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response

class HelpIntentHandler(AbstractRequestHandler):
    """Handler for Help Intent."""
    # Provided by Amazon, customized for this Skill
    def can_handle(self, handler_input):
        return is_intent_name("AMAZON.HelpIntent")(handler_input)

    # Differentiate reply depending on whether user is in-game or outside
    def handle(self, handler_input):
        if gamekeeper.game_is_on:
            speech_text = utterances.utt_meta_helpingame
        else:
            speech_text = utterances.utt_meta_helpoutside

        handler_input.response_builder.speak(speech_text).ask(
            speech_text).set_card(SimpleCard(
                "Albatross", speech_text))
        return handler_input.response_builder.response


class CancelOrStopIntentHandler(AbstractRequestHandler):
    """Single handler for Cancel and Stop Intent."""
    # Provided by Amazon
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return (is_intent_name("AMAZON.CancelIntent")(handler_input) or
                is_intent_name("AMAZON.StopIntent")(handler_input))

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        speech_text = utterances.utt_meta_quit

        handler_input.response_builder.speak(speech_text).set_card(
            SimpleCard("Albatross", speech_text))
        return handler_input.response_builder.response


class FallbackIntentHandler(AbstractRequestHandler):
    """AMAZON.FallbackIntent is only available in en-US locale.
    This handler will not be triggered except in that locale,
    so it is safe to deploy on any locale.
    """
    # Provided by Amazon
    def can_handle(self, handler_input):
        return is_intent_name("AMAZON.FallbackIntent")(handler_input)

    def handle(self, handler_input):
        speech_text = utterances.utt_meta_cannothelp
        reprompt = utterances.utt_meta_helpreprompt
        handler_input.response_builder.speak(speech_text).ask(reprompt)
        return handler_input.response_builder.response


class SessionEndedRequestHandler(AbstractRequestHandler):
    """Handler for Session End."""
    # Provided by Amazon
    def can_handle(self, handler_input):
        # type: (HandlerInput) -> bool
        return is_request_type("SessionEndedRequest")(handler_input)

    def handle(self, handler_input):
        # type: (HandlerInput) -> Response
        return handler_input.response_builder.response


class CatchAllExceptionHandler(AbstractExceptionHandler):
    """Catch all exception handler, log exception and
    respond with custom message.
    """
    # Provided by Amazon
    def can_handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> bool
        return True

    def handle(self, handler_input, exception):
        # type: (HandlerInput, Exception) -> Response
        logger.error(exception, exc_info=True)

        # If user input cannot be understood, inform the user of this
        speech = utterances.utt_meta_exception
        handler_input.response_builder.speak(speech).ask(speech)

        return handler_input.response_builder.response

# Add all handlers to be usable by the Skill
sb.add_request_handler(LaunchRequestHandler())
sb.add_request_handler(StartGameIntentHandler())
sb.add_request_handler(ExitGameIntentHandler())
sb.add_request_handler(SaveGameIntentHandler())
sb.add_request_handler(WhereAmIIntentHandler())
sb.add_request_handler(WhoAmIIntentHandler())
sb.add_request_handler(WhatHappenedIntentHandler())
sb.add_request_handler(WhatIsMyGoalIntentHandler())
sb.add_request_handler(WhoAreYouIntentHandler())
sb.add_request_handler(HowToActivateIntentHandler())
sb.add_request_handler(WhereAreTheKeysIntentHandler())
sb.add_request_handler(WhereAreYouIntentHandler())
sb.add_request_handler(KeysFoundIntentHandler())
sb.add_request_handler(ActivateCommsIntentHandler())
sb.add_request_handler(PickUpKeyIntentHandler())
sb.add_request_handler(GoToRoomIntentHandler())
sb.add_request_handler(HelpIntentHandler())
sb.add_request_handler(CancelOrStopIntentHandler())
sb.add_request_handler(FallbackIntentHandler())
sb.add_request_handler(SessionEndedRequestHandler())
sb.add_exception_handler(CatchAllExceptionHandler())

# Expose the lambda handler to register in AWS Lambda.
lambda_handler = sb.lambda_handler()
