# -*- coding: utf-8 -*-

# University of Potsdam
# Semester Project
# S. Rauhut

"""This is where the Gamekeeper resides. The gamekeeper class keeps track of the game state"""

# Gamekeeper class
class Gamekeeper:
    # Initialization of all game variables
    def __init__(self):
        # Initialize in-game variable with False
        # This variable stores whether the game is currently running or not
        # Starting the game within the skill sets the variable to True
        # Winning or stopping the game sets it to False
        self.game_is_on = False
        # This variable tells the game whether the player has found all three keys
        # This has to be the case in order to win the game
        self.found_lab = False
        self.found_engine = False
        self.found_quarters = False
        # This variable keeps a list of all keys found
        self.keys_found = []
        # This variable will only be set to True if all keys have been found
        self.found_all = False

        # These constants keep strings that will be used in utterances
        self.BRIDGE = "bridge"
        self.LAB = "laboratory"
        self.ENGINE_ROOM = "engine room"
        self.SICKBAY = "sickbay"
        self.QUARTERS = "commander's quarters"
        self.BATHROOM = "bathroom"
        # A list of all rooms on the ship
        self.ALL_ROOMS = [self.LAB, self.BRIDGE, self.ENGINE_ROOM, self.SICKBAY, self.QUARTERS, self.BATHROOM]

        self.LAB_KEY = "the key from the " + self.LAB
        self.ENGINE_KEY = "the key from the " + self.ENGINE_ROOM
        self.QUARTERS_KEY = "the key from the " + self.QUARTERS

        # Set Julia's initial location to the bridge
        # This variable will be updated whenever Julia moves to a new room
        self.drone_location = self.BRIDGE

    # A method that will be called whenever Julia picks up a new key
    def pickupkey(self, key):
        # Make sure the key has not been found already
        # This is merely a safety measure
        # The player should never be able to pick up the same key twice in the first place
        if key not in self.keys_found:
            # If the key is new, add it to the list of found keys
            self.keys_found.append(key)
        # If all three keys have been found, set found_all variable to True
        if len(self.keys_found) == 3:
            self.found_all = True

    # This method will be called whenever the game is won or otherwise ended
    # Its purpose is to reset the game state completely so the player can start anew
    def clearall(self):
        self.found_all = False
        self.keys_found = []
        self.found_lab = False
        self.found_engine = False
        self.found_quarters = False
        self.drone_location = self.BRIDGE
